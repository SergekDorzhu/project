import Vue from 'vue'
import Vuex from 'vuex'
import Profile from './profile/'
Vue.use(Vuex)
export default new Vuex.Store({
 modules: {
   // modules
   Profile
 }
})
