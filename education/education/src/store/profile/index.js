import Axios from 'axios'
// export default {
//     namespaced: true,
//     actions: {
//         loadProfile(ctx,userId) {
//             return new Promise((resolve, reject) => {
//                 Axios
//                     .get("https://jsonplaceholder.typicode.com/users/"+userId, {})
//                     .then(function(response) {
//                         ctx.commit("setProfile", response.data)
//                         resolve(response)
//                     })
//                     .catch(function(error) {
//                         console.log(error);
//                         reject(error.response.data.errors)
//                     });
//             })
//         },
//     },
//     mutations: {
//         setProfile(state, profile) {
//             state.profile = profile
//         },
 
//     },
//     state: {
//         profile: {}
//     },
//     getters: {
//         profile: (state) => state.profile,
//     }
// }

export default {
    namespaced: true,
    actions: {
        loadProfile(ctx) {
            return new Promise((resolve, reject) => {
                Axios
                .get("https://jsonplaceholder.typicode.com/posts/", {})
                .then(function (response) {
                    ctx.commit("setProfile", response.data)
                    resolve(response);
                })
                .catch(function (error) {
                    console.log(error);
                    reject(error.response.data.errors);
                });
            })
        },
    },
    mutations: {
        setProfile(state, profile) {
            state.profile = profile
        },
 
    },
    state: {
        profile: {}
    },
    getters: {
        profile: (state) => state.profile,
        getInfo:(state)=>{
            return state.profile;
        },
        getUserId: (state) => {
            return state.profile.userId;
        },
        getTitle: (state) => {
            return state.profile.title;
        },

    }
}
 
 