import Vue from 'vue'
import VueRouter from 'vue-router'
Vue.use(VueRouter)
const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/first',
      name: 'first-page',
      component: () => import('@/pages/FirstPage')
    },
    {
        path: '/table',
        name: 'table',
        component: ()=>import('@/pages/Table')
    },
    {
      path: '/profile/:id',
      name: 'profile',
      component: ()=>import('@/pages/Profile')
    },
  ]
})
export default router
